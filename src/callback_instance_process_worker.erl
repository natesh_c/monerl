%%%-------------------------------------------------------------------
%%% @author tringapps
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 11. Mar 2016 6:38 PM
%%%-------------------------------------------------------------------
-module(callback_instance_process_worker).
-author("tringapps").

-behaviour(instance_process_worker).

%% API
-export([init/1,do_before/1,do/2,do_after/2,handle_cast/2,handle_call/2,on_terminate/2]).

%%%===================================================================
%%% API
%%%===================================================================


%%%===================================================================
%%% process_worker callbacks
%%%===================================================================

-spec init(InitalArguments::any()) -> {ok,State::any()}.

init(_InitalArguments) -> {ok,[]}.

-spec do_before(State::term()) -> {retry,State::any()} | {continue,NextLevelArguments::any(),State::term()}
| {kill,Reason::any(),State::term()}.

do_before(State) -> {continue,[],State}.

-spec do(Arguments::any(),State::term()) -> {retry,Args::any(),State::any()} | {continue,ResultSet::any(),State::term()} |
{kill,Reason::any(),State::term()}.

do(_Argumrents,State) -> {retry,State}.

-spec do_after(Arguments::any(),State::term()) -> {retry,Arguments::any(),State::term()} | {next,State::term()}
| {next_do,State::term()} | {wait,Time::timeout(),State::term()} | {kill,Reason::any(),State::term()}.

do_after(_Arguments,State) -> {next,State}.

-spec on_terminate(Reason::any(),State::term()) -> ok.
on_terminate(_Reason,_State) -> ok.

-spec handle_cast(Content::any(),State::any()) -> {noreply,State::any()} |
{stop,Reason::term(),State::any()}.

handle_cast(_Content,State) -> {noreply,State}.

-spec handle_call(Content::any(),State::any()) -> {reply,Reply::term(),State::any()} |
{stop,Reason::term(),State::any()}.

handle_call(_Content,State) -> {reply,ok,State}.

%%%===================================================================
%%% internal functions
%%%===================================================================







