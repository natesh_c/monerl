%%%-------------------------------------------------------------------
%%% @author tringapps
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 10. Mar 2016 4:52 PM
%%%-------------------------------------------------------------------
-module(instance_process_state).
-author("tringapps").

-behaviour(gen_server).

%% API
-export([start_link/1,can_create_process/0,process_terminated/1,alter_max_process_count/1,process_created/1]).

%% gen_server callbacks
-export([init/1,handle_call/3,handle_cast/2,handle_info/2,terminate/2,code_change/3,format_status/2]).

-record(state, {max_process_count=0,current_process_count=0,process_list=[]}).

%%%===================================================================
%%% API
%%%===================================================================

start_link(MaxprocessCount) ->  gen_server:start_link({local, ?MODULE}, ?MODULE, [MaxprocessCount], []).

can_create_process() -> gen_server:call(?MODULE,is_process_avail).

process_terminated(Pid) -> gen_server:cast(?MODULE,{process_terminated,Pid}).

process_created(Pid) -> gen_server:cast(?MODULE,{process_created,Pid}).

alter_max_process_count(NewCount) -> gen_server:cast(?MODULE,{alter_process_count,NewCount}).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

init([MaxprocessCount]) ->  {ok, #state{max_process_count = MaxprocessCount}}.

handle_call(is_process_avail,_,#state{max_process_count = Max,current_process_count = Current}= State) when Max > Current ->
  {reply, yes, State};

handle_call(is_process_avail, _From, State) ->
  {reply, no, State};

handle_call(_Request, _From, State) ->
  {reply, ok, State}.

handle_cast({process_created,Pid}, #state{current_process_count = Current,process_list = PList} = State) ->
  {noreply, State#state{current_process_count = Current +1,process_list = [Pid|PList]}};

handle_cast({process_terminated,Pid}, #state{current_process_count = Current,process_list = PList} = State) ->
  {noreply, State#state{current_process_count = Current -1,process_list = lists:delete(Pid,PList)}};

handle_cast({alter_process_count,NewCount}, State) ->
  {noreply, State#state{max_process_count = NewCount}};

handle_cast(_Request, State) ->
  {noreply, State}.

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

format_status(_,_) -> ok.

%%%===================================================================
%%% Internal functions
%%%===================================================================



