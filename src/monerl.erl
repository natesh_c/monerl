%%%-------------------------------------------------------------------
%%% @author tringapps
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 11. Mar 2016 12:26 PM
%%%-------------------------------------------------------------------
-module(monerl).
-author("tringapps").

-behaviour(gen_server).

%% API
-export([start_link/1,start_work/3,create_temp_process/0,create_permanent_process/0,make_call/2,make_cast/2]).
-export([make_cast_callback/3]).

%% gen_server callbacks
-export([init/1,handle_call/3,handle_cast/2,handle_info/2,terminate/2,code_change/3,format_status/2]).

-define(SERVER, ?MODULE).

-record(state, {max_process = 500,count_state,process_list=[]}).

%%%===================================================================
%%% API
%%%===================================================================

start_link(MaxProcessCount) -> gen_server:start_link({local, ?SERVER}, ?MODULE, [MaxProcessCount], []).

create_temp_process() -> create_process(create_temp,instance_process_state:can_create_process()).

create_permanent_process() -> create_process(create_permanent,instance_process_state:can_create_process()).

start_work(Pid,CallBackMod,Args) -> instance_process_worker:set_callback(Pid,CallBackMod,Args,[]).

make_call(Pid,Data) -> instance_process_worker:call(Pid,Data).

make_cast(Pid,Data) -> instance_process_worker:cast(Pid,Data).

make_cast_callback(Pid,Data,CallBackDetails) -> instance_process_worker:castback(Pid,Data,CallBackDetails).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

init([MaxProcessCount]) ->
  {ok, #state{max_process = MaxProcessCount,count_state = instance_process_state:start_link(MaxProcessCount)}}.

handle_call(create_temp, _From, State) ->
  {reply,create_process(create_temp,instance_process_state:can_create_process()) , State};

handle_call(create_permanent, _From, State) ->
  {reply,create_process(create_permanent,instance_process_state:can_create_process())  ,State};

handle_call(_Request, _From, State) ->  {reply, ok, State}.

handle_cast(_Request, State) ->  {noreply, State}.

handle_info(_Info, State) ->  {noreply, State}.

terminate(_Reason, _State) ->  ok.

code_change(_OldVsn, State, _Extra) ->  {ok, State}.

format_status(_,_) -> ok.

%%%===================================================================
%%% Internal functions
%%%===================================================================

create_process(create_permanent,yes) -> instance_process_worker:create([{mode,permanent},{max_retry,5}]);

create_process(create_temp,yes) -> instance_process_worker:create([{mode,temporary},{max_retry,3}]);

create_process(_,no) -> not_allow.

