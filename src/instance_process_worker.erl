%%%-------------------------------------------------------------------
%%% @author tringapps
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 10. Mar 2016 6:01 PM
%%%-------------------------------------------------------------------
-module(instance_process_worker).
-author("tringapps").

-behaviour(gen_server).

%% API
-export([create/1,set_callback/4,call/2,cast/2,castback/3]).

%% gen_server callbacks
-export([init/1,handle_call/3,handle_cast/2,handle_info/2,terminate/2,code_change/3,format_status/2]).

-record(state,{callbackmodule,callbackmodulestate,mode = temporary,terminate_reason}).

%%%===================================================================
%%% CallBacks
%%%===================================================================

-callback init(InitalArguments::any()) -> {ok,State::any()}.

-callback do_before(State::term()) -> {retry,State::any()} | {continue,NextLevelArguments::any(),State::term()}
| {stop,Reason::any(),State::term()}.

-callback do(Arguments::any(),State::term()) -> {retry,State::any()} | {continue,ResultSet::any(),State::term()} |
{stop,Reason::any(),State::term()}.

-callback do_after(Arguments::any(),State::term()) -> {retry,Arguments::any(),State::term()} |{must_retry,Arguments::any(),State::term()} | {next,State::term()}
| {next_do,Args::term(),State::term()} | {wait,Time::timeout(),State::term()} | {stop,Reason::any(),State::term()}.

-callback on_terminate(Reason::any(),State::term()) -> {ok,State::term()}.

-callback handle_call(Content::any(),State::any()) -> {reply,Reply::term(),State::any()} |
{stop,Reason::term(),State::any()}.

-callback handle_cast(Content::any(),State::any()) -> {noreply,State::any()} |
{stop,Reason::term(),State::any()}.

%%%===================================================================
%%% API
%%%===================================================================

create(Options) -> gen_server:start_link(?MODULE, Options,[]).

set_callback(Pid,CallBackMod,Args,_Options) -> gen_server:cast(Pid,{set_callback,CallBackMod,Args}).

castback(Pid,Content, CallBackDetails) -> gen_server:cast(Pid,{castback,Content,CallBackDetails}).

call(Pid,Data) -> gen_server:call(Pid,{call_action,Data}).

cast(Pid,Data) -> gen_server:cast(Pid,{cast_action,Data}).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

init(Options) ->  instance_process_state:process_created(self()),  {ok,save_options(Options)}.

handle_call({call_action,Data},_From,#state{callbackmodule = CMod,callbackmodulestate = CState}=State) ->
  handle_call_response(CMod:handle_call(Data,CState),State);

handle_call(_Request, _From, State) ->  {reply, ok, State}.

handle_cast({set_callback,CallBackModule,InitalArguments},State) ->
  {ok,CState}  = CallBackModule:init(InitalArguments), cast_do_before(),
  {noreply,State#state{callbackmodule = CallBackModule,callbackmodulestate = CState}};

handle_cast(do_before, #state{callbackmodule = Mod,callbackmodulestate = ModState}=State) ->
  handle_do_before_response(Mod:do_before(ModState),State);

handle_cast({do,Arguments}, #state{callbackmodule = Mod,callbackmodulestate = ModState}=State) ->
  handle_do_response(Mod:do(Arguments,ModState),State);

handle_cast({do_after,Arguments}, #state{callbackmodule = Mod,callbackmodulestate = ModState}=State) ->
  handle_do_after_response(Mod:do_after(Arguments,ModState),State);

handle_cast({castback,Content,CallBackDetails},#state{callbackmodule = CMod,callbackmodulestate = CState}=State) ->
  handle_castback_response(CMod:handle_call(Content,CState),CallBackDetails,State);

handle_cast({cast_action,Content},#state{callbackmodule = CMod,callbackmodulestate = CState}=State) ->
  handle_cast_response(CMod:handle_cast(Content,CState),State);

handle_cast(_Request, State) ->  {noreply, State}.

handle_info(_Info, State) ->  {noreply, State}.

terminate(normal,#state{callbackmodule = Mod,callbackmodulestate = ModState,terminate_reason = Reason} = _State) ->
  instance_process_state:process_terminated(self()),
  Mod:on_terminate(Reason,ModState),
  io:format("terminated ~n"),
  ok;

terminate(Status,_State) -> io:format("terminated ~p~n",[Status]),instance_process_state:process_terminated(self()),ok.

code_change(_OldVsn, State, _Extra) ->  {ok, State}.

format_status(_,_) -> ok.

%%%===================================================================
%%% Internal functions
%%%===================================================================

cast_do_before() -> gen_server:cast(self(),do_before).

cast_do(Arguments) -> gen_server:cast(self(),{do,Arguments}).

cast_do_after(Arguments) -> gen_server:cast(self(),{do_after,Arguments}).

handle_do_before_response({retry,ModState},#state{mode = permanent}=State)-> cast_do_before(),{noreply,State#state{callbackmodulestate = ModState}};
handle_do_before_response({retry,ModState},#state{mode = temporary }=State)->  {stop,normal,State#state{callbackmodulestate = ModState,terminate_reason = normal}};
handle_do_before_response({continue,Arguments,ModState},State) ->  cast_do(Arguments),{noreply,State#state{callbackmodulestate = ModState}};
handle_do_before_response({stop,Reason,ModState},State) -> {stop,normal,State#state{callbackmodulestate = ModState,terminate_reason = Reason}};
handle_do_before_response(Any,_State) -> erlang:throw({wrong_response,Any}).

handle_do_response({retry,Arguments,ModState},State)-> cast_do(Arguments),{noreply,State#state{callbackmodulestate = ModState}};
handle_do_response({continue,ResultSet,ModState},State)  ->  cast_do_after(ResultSet),{noreply,State#state{callbackmodulestate = ModState}};
handle_do_response({stop,Reason,ModState},#state{mode = temporary} = State) ->  {stop,normal,State#state{callbackmodulestate = ModState,terminate_reason = Reason}};
handle_do_response({stop,_Reason,ModState},#state{mode = permanent} = State) ->  cast_do_before(),{noreply,State#state{callbackmodulestate = ModState}};
handle_do_response(Any,_State)  -> erlang:throw({wrong_response,Any}).

handle_do_after_response({retry,Arguments,ModState},#state{} = State) ->  cast_do_after(Arguments),
  {noreply,State#state{callbackmodulestate = ModState}};
handle_do_after_response({next,ModState},State) ->  cast_do_before(), {noreply,State#state{callbackmodulestate = ModState}};
handle_do_after_response({next_do,Arugments,ModState},State) ->  cast_do(Arugments), {noreply,State#state{callbackmodulestate = ModState}};
handle_do_after_response({wait,WaitingTimestamp,ModState},State) -> timer:sleep(WaitingTimestamp), cast_do_before(),
  {noreply,State#state{callbackmodulestate = ModState}};
handle_do_after_response({stop,Reason,ModState},#state{mode = temporary} = State) ->  {stop,normal,State#state{callbackmodulestate = ModState,terminate_reason = Reason}};
handle_do_after_response({stop,_Reason,ModState},#state{mode = permanent} = State) ->  cast_do_before(),
  {noreply,State#state{callbackmodulestate = ModState}};
handle_do_after_response(Any,_State)  -> erlang:throw({wrong_response,Any}).

handle_call_response({reply,Reply,CState},State) -> {reply,Reply,State#state{callbackmodulestate = CState}};
handle_call_response({stop,_Reply,Reason,ModState},#state{mode = temporary} = State) ->  {stop,normal,State#state{callbackmodulestate = ModState,terminate_reason = Reason}};
handle_call_response({stop,_,_Reason,ModState},#state{mode = permanent} = State) ->  cast_do_before(),
  {noreply,State#state{callbackmodulestate = ModState}};
handle_call_response(Any,_State)  -> erlang:throw({wrong_response,Any}).

handle_cast_response({noreply,CState},State) -> {noreply,State#state{callbackmodulestate = CState}};
handle_cast_response({stop,Reason,ModState},State) ->  {stop,normal,State#state{callbackmodulestate = ModState,terminate_reason = Reason}};
handle_cast_response(Any,_State)  -> erlang:throw({wrong_response,Any}).

handle_castback_response({reply,Reply,CState},CallBack,State) -> send_back(CallBack,Reply), {noreply,State#state{callbackmodulestate = CState}};
handle_castback_response({stop,Reason,ModState},_,State) ->  {stop,normal,State#state{callbackmodulestate = ModState,terminate_reason = Reason}};
handle_castback_response(Any,_,_State)  -> erlang:throw({wrong_response,Any}).

send_back({mod,Module,Function,Args},Response) -> Module:Function(Response,Args);
send_back({mod,Module,Function},Response) -> Module:Function(Response);
send_back({function,Fun},Response)when is_function(Fun) -> Fun(Response);
send_back(_Any,_Response) -> no_need.

%%%===================================================================
%%% Helper functions
%%%===================================================================

save_options(Options)-> save_options(Options,#state{}).
save_options([],State) -> State;
save_options([{mode,temporary}|Rest],State) -> save_options(Rest,State#state{mode = temporary});
save_options([{mode,permanent}|Rest],State) -> save_options(Rest,State#state{mode = permanent});
save_options([_|Rest],State) -> save_options(Rest,State).

